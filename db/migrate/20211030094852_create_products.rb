class CreateProducts < ActiveRecord::Migration[6.1]
  def change
    create_table :products do |t|
      t.string :name
      t.float :price, default: 0.0
      t.integer :combo, default: 0
      t.float :discount, default: 0.0
      t.timestamps
    end
  end
end
