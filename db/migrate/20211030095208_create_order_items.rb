class CreateOrderItems < ActiveRecord::Migration[6.1]
  def change
    create_table :order_items do |t|
      t.references :product, foreign_key: true
      t.references :order, foreign_key: true
      t.string :product_name
      t.float :rate, default: 0.0
      t.integer :quantity, default: 0
      t.float :discount, default: 0.0
      t.float :amount, default: 0.0
      t.timestamps
    end
  end
end
