class CreateOrders < ActiveRecord::Migration[6.1]
  def change
    create_table :orders do |t|
      t.date :date_of_order
      t.boolean :confirm_order, default: false
      t.string :coupon_code
      t.timestamps
    end
  end
end
