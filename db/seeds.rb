# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
[
  {
    "name": "Purple carrot",
    "price": 30,
    "combo": 0,
    "discount": 0
  },
  {
    "name": "Bread",
    "price": 50,
    "combo": 2,
    "discount": 10
  },
  {
    "name": "Egg",
    "price": 10,
    "combo": 10,
    "discount": 10
  },
  {
    "name": "Sweet Potato",
    "price": 15,
    "combo": 0,
    "discount": 0
  },
  {
    "name": "Asian Greens",
    "price": 300,
    "combo": 0,
    "discount": 0
  },
  {
    "name": "Cookies",
    "price": 250,
    "combo": 20,
    "discount": 30
  },
  {
    "name": "Cashew nuts",
    "price": 500,
    "combo": 0,
    "discount": 0
  },
  {
    "name": "Almonds",
    "price": 500,
    "combo": 0,
    "discount": 0
  },
  {
    "name": "Cranberry",
    "price": 200,
    "combo": 0,
    "discount": 0
  },
  {
    "name": "Blackberries",
    "price": 500,
    "combo": 5,
    "discount": 100
  }
].each {|item| Product.create(item)}