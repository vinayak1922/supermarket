require "test_helper"

class Orders::OrderItemsControllerTest < ActionDispatch::IntegrationTest

  test 'create' do
    assert_difference 'OrderItem.count' do
      post order_order_items_path(orders(:order_one)), params: { order_item: { product_id: products(:sweet_potato).id, quantity: 10 } }
    end
  end

  test 'update' do
    assert_equal 100, order_item_one.amount
    assert_equal 2, order_item_one.quantity
    assert_equal 10, order_item_one.discount
    put order_order_item_path(orders(:order_one), order_item_one), params: { order_item: { quantity: 5 } }
    assert_equal 250, order_item_one.reload.amount
    assert_equal 5, order_item_one.reload.quantity
    assert_equal 20, order_item_one.reload.discount
  end

  test 'destroy' do
    assert_difference 'OrderItem.count', -1 do
      delete order_order_item_path(orders(:order_one), order_item_one)
    end
  end

  private

  def order_item_one
    order_items(:order_item_one)
  end
end
