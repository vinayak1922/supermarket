require "test_helper"

class OrdersControllerTest < ActionDispatch::IntegrationTest
  test 'index' do
    get root_path
    assert_response :success
  end

  test 'create' do
    assert_difference 'Order.count' do
      post orders_path
    end
  end

  test 'edit' do
    get edit_order_path(orders(:order_one))
    assert_response :success
  end

  test 'update' do
    assert_nil orders(:order_one).coupon_code
    put order_path(orders(:order_one)), params: { order: { coupon_code: 'ORDER10' } }
    assert_equal 'ORDER10', orders(:order_one).reload.coupon_code
  end

  test 'destroy' do
    assert_difference 'Order.count', -1 do
      delete order_path(orders(:order_one))
    end
  end

  test 'confirm_order' do
    refute orders(:order_one).confirm_order
    assert_nil orders(:order_one).date_of_order
    put confirm_order_path(orders(:order_one))
    assert orders(:order_one).reload.confirm_order
    assert_equal Date.today, orders(:order_one).reload.date_of_order
  end
end
