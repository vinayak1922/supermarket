Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'orders#index'
  resources :products, only: [:index]
  put 'orders/:id/confirm_order', to: 'orders#confirm_order', as: :confirm_order
  resources :orders, except: [:index] do
    scope module: :orders do
      resources :order_items, only: [:create, :update, :destroy]
    end
  end
end
