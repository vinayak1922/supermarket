class OrderItem < ApplicationRecord
  belongs_to :product
  belongs_to :order

  validates :quantity, presence: true
  validates_uniqueness_of :product_id, scope: [:order_id]
  before_create :set_product_details
  before_save :set_discount

  def bill_amount
    amount - discount
  end


  def set_product_details
    self.product_name = product.name
    self.rate = product.price
    set_amount
  end

  def set_discount
    set_amount
    combo = product.combo
    if quantity >= combo && combo > 0
      self.discount = (quantity / combo).to_i * product.discount
    end
  end

  def set_amount
    self.amount = rate * quantity
  end
end
