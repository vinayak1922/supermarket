class Order < ApplicationRecord
  has_many :order_items, dependent: :delete_all

  def total_amount
    order_items.map { |i| i.bill_amount }.sum
  end

  def bill_amount
    total_amount - coupon_discount
  end

  def coupon_discount
    if coupon_code == 'ORDER10'
      total_amount * 0.10
    else
      0
    end
  end
end
