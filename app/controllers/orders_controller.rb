class OrdersController < ApplicationController
  before_action :set_order, except: [:index, :create]
  def index
    @orders = Order.order(created_at: :desc)
  end

  def create
    @order = Order.create
    redirect_to edit_order_path(@order)
  end

  def edit

  end

  def show

  end

  def update
    @order.update(order_params)
    redirect_to order_path(@order)
  end

  def destroy
    @order.destroy
    redirect_to root_path
  end

  def confirm_order
    @order.update(confirm_order: true, date_of_order: Date.today)
    redirect_to order_path(@order)
  end

  private

  def set_order
    @order = Order.find_by_id params[:id]
  end

  def order_params
    params.require(:order).permit(:coupon_code)
  end
end
