class Orders::OrderItemsController < ApplicationController
  before_action :set_order
  before_action :set_order_item, only: [:destroy, :update]

  def create
    @order_item = @order.order_items.create(order_item_params)
    if @order_item.errors.blank?
      redirect_to edit_order_path(@order)
    end
  end

  def update
    @order_item.update(order_item_params)
    redirect_to edit_order_path(@order)
  end

  def destroy
    @order_item.destroy
    redirect_to edit_order_path(@order)
  end

  private

  def set_order
    @order = Order.find_by_id params[:order_id]
  end

  def set_order_item
    @order_item = @order.order_items.find_by_id params[:id]
  end

  def order_item_params
    params.require(:order_item).permit(:product_id, :quantity)
  end
end
